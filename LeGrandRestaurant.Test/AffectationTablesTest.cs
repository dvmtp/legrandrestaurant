using Xunit;


namespace LeGrandRestaurant.Test
{
    public class AffectationTablesTest
    {
        [Fact(DisplayName = "Etant donné une salle ayant une table, elle est affectée par défaut au maitre d'hotel. ")]
        public void Affectation_Initiale()
        {
            //etant donné une salle ayant une table
            var maitreHotel = new MaitreHotel();
            var table = new Table();
            new Salle(maitreHotel, table);
            
            // Alors cette table est affectee par defaut au maitre d'hotel
            Assert.Equal(maitreHotel, table.Affectataire);
        }

        [Fact(DisplayName = "Etant donné une salle ayant deux tables, l'affectation de l'une d'entre" +
                            "Elle a un serveur ne change que son affectation.")]
        public void Deux_Tables_Une_Est_Affectée_A_Autrui()
        {
            var maitreHotel = new MaitreHotel();

            var tableMaitreHotel = new Table();
            var tableDuServeur = new Table();

            new Salle(maitreHotel, tableDuServeur, tableMaitreHotel);

            var serveur = new Serveur();
            tableDuServeur.AffecterA(serveur);
            
            Assert.Equal(serveur, tableDuServeur.Affectataire);
            Assert.Equal(maitreHotel, tableMaitreHotel.Affectataire);
        }
    }
}