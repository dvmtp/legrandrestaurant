namespace LeGrandRestaurant
{
    public class Salle
    {

        public Salle(MaitreHotel maitreHotel, params Table[] tables)
        {
            foreach (var table in tables)
            {
                table.AffecterA(maitreHotel);
            }
        }
    }
}